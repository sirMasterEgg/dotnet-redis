using System.Security.Cryptography;
using System.Text;
using dotnet8.Databases;
using dotnet8.Databases.Models;
using dotnet8.DTO;
using dotnet8.DTO.Auth;
using dotnet8.Static;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace dotnet8.Controllers
{
    [ApiController]
    [Route($"{Variable.PREFIX_URL}/auth/")]
    public class AuthController : ControllerBase
    {
        private readonly IDistributedCache _cache;
        private readonly ApplicationDbContext _context;
        public AuthController(IDistributedCache cache, ApplicationDbContext context)
        {
            _cache = cache;
            _context = context;
        }

        private string CreateKey()
        {
            var salt = new byte[16];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            StringBuilder sb = new StringBuilder();
            foreach (byte b in salt)
            {
                sb.Append(b.ToString("x2"));
            }
            return sb.ToString();
        }

        [HttpPost("login")]
        public async Task<ActionResult> Login([FromBody] LoginRequest loginDto)
        {
            var user = await _context.Users.Where(user => user.Email == loginDto.Email).FirstOrDefaultAsync();
            
            if (user == null)
            {
                throw new UnauthorizedAccessException("Invalid credentials");
            }

            var isValidPassword = BCrypt.Net.BCrypt.Verify(loginDto.Password, user.Password);

            if (!isValidPassword)
            {
                throw new UnauthorizedAccessException("Invalid credentials");
            }

            try
            {
                var key = CreateKey();
                var userSerialized = JsonConvert.SerializeObject(user);

                _cache.Set(key, System.Text.Encoding.UTF8.GetBytes(userSerialized));
                //var value = _cache.Get("a");
                //var result = System.Text.Encoding.UTF8.GetString(value);
                return Ok(new LoginResponse()
                {
                    Token = key,
                    Email = user.Email
                });

            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpGet("logout")]
        public async Task<ActionResult> Logout()
        {
            var tokenKey = Request.Headers["X-SESSION-ID"][0] ?? "";

            var value = await _cache.GetAsync(tokenKey);
            
            if (value == null)
            {
                throw new UnauthorizedAccessException();
            }

            try
            {
                await _cache.RemoveAsync(tokenKey);
                return Ok(new LogoutResponse()
                {
                    Message = "Logged out successfully!",
                });
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpPost("register")]
        public async Task<ActionResult> Register([FromBody] RegisterRequest registerDto)
        {
            var existUser = await _context.Users.Where(user => user.Email == registerDto.Email).FirstOrDefaultAsync();
            if (existUser != null)
            {
                throw new BadHttpRequestException("Email already exist!");
            }

            var user = new User()
            {
                Email = registerDto.Email,
                Password = BCrypt.Net.BCrypt.HashPassword(registerDto.Password)
            };

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            var response = new RegisterResponse()
            {
                Email = user.Email,
                Id = user.Id,
            };

            return Created("",response);
        }
    }
}
