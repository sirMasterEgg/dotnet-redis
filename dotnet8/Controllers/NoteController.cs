﻿using dotnet8.Databases;
using dotnet8.Databases.Models;
using dotnet8.DTO.Note;
using dotnet8.Exceptions;
using dotnet8.Static;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System.Diagnostics;

namespace dotnet8.Controllers
{
    [ApiController]
    [Route($"{Variable.PREFIX_URL}/note/")]
    public class NoteController : ControllerBase
    {
        private readonly IDistributedCache _cache;
        private readonly ApplicationDbContext _context;
        public NoteController(IDistributedCache cache, ApplicationDbContext context)
        {
            _cache = cache;
            _context = context;
        }

        private async Task<User> GetCurrentUser()
        {
            var tokenKey = Request.Headers["X-SESSION-ID"][0] ?? "";

            // berapa lama redis mengambil data
            Stopwatch timer = new Stopwatch();
            timer.Start();

            var value = await _cache.GetAsync(tokenKey);
            
            timer.Stop();
            Console.WriteLine(timer.Elapsed.TotalSeconds);

            if (value == null)
            {
                throw new UnauthorizedAccessException();
            }

            var stringifyUser = System.Text.Encoding.UTF8.GetString(value);

            var user = JsonConvert.DeserializeObject<User>(stringifyUser);

            if (user == null)
            {
                throw new InvalidDataException("Error on parse user");
            }

            return user;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var user = await GetCurrentUser();

            var notes = _context.Notes.Where(note => note.UserId == user.Id).Select(note => new NoteProperty()
            {
                Id = note.Id,
                Title = note.Title,
                Content = note.Content
            }).ToList();


            return Ok(new GetAllNoteResponse()
            {
                Notes = notes,
            });
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var user = await GetCurrentUser();
            var note = await _context.Notes.FindAsync(id);

            if (note != null && note.UserId != user.Id)
            {
                throw new ForbiddenException("You cannot access this property");
            }

            if (note == null)
            {
                throw new NotFoundException("Resource not found");
            }

            return Ok(new GetNoteResponse()
            {
                Note = new NoteProperty()
                {
                    Id = note.Id,
                    Title = note.Title,
                    Content = note.Content
                }
            });
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] CreateNoteRequest noteDto)
        {
            var user = await GetCurrentUser();

            if (user == null)
            {
                throw new UnauthorizedAccessException();
            }

            var note = new Note()
            {
                Title = noteDto.Title,
                Content = noteDto.Content,
                UserId = user.Id
            };
            _context.Notes.Add(note);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return Created("", new CreateNoteResponse()
            {
                Id = note.Id,
                Title = note.Title,
                Content = note.Content,
                UserId = user.Id
            });
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] UpdateNoteRequest noteDto)
        {
            var user = await GetCurrentUser();
            var note = await _context.Notes.FindAsync(id);

            if (note != null && note.UserId != user.Id)
            {
                throw new ForbiddenException("You cannot access this property");
            }

            if (note == null)
            {
                throw new NotFoundException("Resource not found");
            }

            if (noteDto.Title != note.Title && noteDto.Title != null)
            {
                note.Title = noteDto.Title;
            }

            if (noteDto.Content != note.Content && noteDto.Content != null)
            {
                note.Content = noteDto.Content;
            }

            await _context.SaveChangesAsync();

            return Ok(new CreateNoteResponse()
            {
                Id = note.Id,
                Title = note.Title,
                Content = note.Content,
                UserId = user.Id
            });
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTask(int id)
        {
            var user = await GetCurrentUser();
            var note = await _context.Notes.FindAsync(id);

            if (note != null && note.UserId != user.Id)
            {
                throw new ForbiddenException("You cannot access this property");
            }

            if (note == null)
            {
                throw new NotFoundException("Resource not found");
            }

            _context.Notes.Remove(note);
            await _context.SaveChangesAsync();

            return Ok(new DeleteNoteResponse()
            {
                Message = "Successfully deleted!"
            });
        }
    }
}
