﻿using dotnet8.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace dotnet8.Databases
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Note> Notes { get; set; }
        
    }
}
