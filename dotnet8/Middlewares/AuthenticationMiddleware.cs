﻿using System.Text.Json;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using dotnet8.Databases.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using System.Net.Mime;
using System.Net;

namespace dotnet8.Middlewares
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IDistributedCache _cache;

        public AuthenticationMiddleware(RequestDelegate next, IDistributedCache cache)
        {
            _next = next;
            _cache = cache;
        }

        public Task Invoke(HttpContext httpContext)
        {
            if (!httpContext.Request.Headers.ContainsKey("X-SESSION-ID"))
            {
                throw new UnauthorizedAccessException();
            }

            var token = httpContext.Request.Headers["X-SESSION-ID"][0] ?? "";

            try
            {
                if (token == null)
                {
                    throw new UnauthorizedAccessException();
                }

                var value = _cache.Get(token);

                if (value == null)
                {
                    throw new UnauthorizedAccessException();
                }
            }
            catch (Exception e)
            {
                throw new UnauthorizedAccessException();
            }
            return _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class AuthenticationMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthenticationMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthenticationMiddleware>();
        }
    }
}
