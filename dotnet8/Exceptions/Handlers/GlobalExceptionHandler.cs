﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using dotnet8.Databases.Models;
using dotnet8.Exceptions.Models;
using Microsoft.AspNetCore.Http.HttpResults;

namespace dotnet8.Exceptions.Handler
{
    public class GlobalExceptionHandler : IExceptionHandler
    {
        public async ValueTask<bool> TryHandleAsync(HttpContext httpContext, Exception exception, CancellationToken cancellationToken)
        {
            var error = new ErrorModel();
            switch (exception)
            {
                case BadHttpRequestException:
                    error.StatusCode = StatusCodes.Status400BadRequest;
                    error.Message = "Bad Request";
                    break;
                case UnauthorizedAccessException:
                    error.StatusCode = StatusCodes.Status401Unauthorized;
                    error.Message = "Unauthorized";
                    break;
                case ForbiddenException:
                    error.StatusCode = StatusCodes.Status403Forbidden;
                    error.Message = "Forbidden";
                    break;
                case NotFoundException:
                    error.StatusCode = StatusCodes.Status404NotFound;
                    error.Message = "Not Found";
                    break;
                default:
                    error.StatusCode = StatusCodes.Status500InternalServerError;
                    error.Message = "Internal Server Error";
                    break;
            }
            error.Details = exception.Message.Length == 0 ? exception.StackTrace : exception.Message;

            httpContext.Response.StatusCode = error.StatusCode;
            await httpContext.Response.WriteAsJsonAsync(error, cancellationToken: cancellationToken);

            return true;
        }
    }
}
