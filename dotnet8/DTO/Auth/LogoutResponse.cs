﻿namespace dotnet8.DTO.Auth
{
    public class LogoutResponse
    {
        public string Message { get; set; }
    }
}
