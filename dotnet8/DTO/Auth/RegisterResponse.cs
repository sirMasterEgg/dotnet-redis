﻿namespace dotnet8.DTO.Auth
{
    public class RegisterResponse
    {
        public int Id { get; set; }
        public string Email { get; set; }
    }
}
