﻿namespace dotnet8.DTO.Auth
{
    public class LoginResponse
    {
        public string Token { get; set; }
        public string Email { get; set; }
    }
}
