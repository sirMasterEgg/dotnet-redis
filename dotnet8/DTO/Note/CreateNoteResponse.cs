﻿namespace dotnet8.DTO.Note
{
    public class CreateNoteResponse
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int UserId { get; set; }
    }
}
