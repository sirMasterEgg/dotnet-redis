﻿namespace dotnet8.DTO.Note
{
    public class GetNoteResponse
    {
        public NoteProperty Note { get; set; }
    }
}
