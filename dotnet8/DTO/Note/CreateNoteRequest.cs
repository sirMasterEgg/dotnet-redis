﻿namespace dotnet8.DTO.Note
{
    public class CreateNoteRequest
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
