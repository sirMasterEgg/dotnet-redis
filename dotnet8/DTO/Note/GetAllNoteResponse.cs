﻿namespace dotnet8.DTO.Note
{
    public class GetAllNoteResponse
    {
        public List<NoteProperty> Notes { get; set; }
    }

    public class NoteProperty
    {
        public int Id{ get; set; }
        public string Title{ get; set; }
        public string Content{ get; set; }
    }
}
