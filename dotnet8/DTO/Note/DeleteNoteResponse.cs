﻿namespace dotnet8.DTO.Note
{
    public class DeleteNoteResponse
    {
        public string Message { get; set; }
    }
}
