﻿namespace dotnet8.DTO.Note
{
    public class UpdateNoteRequest
    {
        public string? Title { get; set; }
        public string? Content { get; set; }
    }
}
